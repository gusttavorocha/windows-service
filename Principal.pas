unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdmPrincipal = class(TService)
    Conn: TFDConnection;
    Query: TFDQuery;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceBeforeUninstall(Sender: TService);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    function BuscaBanco (fCod : integer; fCampo: string): String;
    { Public declarations }
  end;

var
  dmPrincipal: TdmPrincipal;

implementation

uses
  LogUtils;

{$R *.dfm}

function TdmPrincipal.BuscaBanco (fCod : integer; fCampo: string): String;
begin
  Query.Close;
  Query.ParamByName('cod').AsInteger:= fCod;
  Query.Open();
  result:= Query.FieldByName(fCampo).AsString;
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  dmPrincipal.Controller(CtrlCode);
end;

function TdmPrincipal.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TdmPrincipal.ServiceAfterInstall(Sender: TService);
begin
  Log('Servi�o Ap�s Instala��o');
  Dados(BuscaBanco(1,'Nome'));
end;

procedure TdmPrincipal.ServiceAfterUninstall(Sender: TService);
begin
  Log('Servi�o Ap�s Desinstala��o');
  Dados(BuscaBanco(3,'Login'));
end;

procedure TdmPrincipal.ServiceBeforeInstall(Sender: TService);
begin
  Log('Servi�o Anterior a Instala��o');
  Dados(BuscaBanco(2,'Senha'));
end;

procedure TdmPrincipal.ServiceBeforeUninstall(Sender: TService);
begin
  Log('Servi�o Anterior a Desinstala��o');
end;

procedure TdmPrincipal.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  Log('Servi�o Continuo');
  Continued:= True;
end;

procedure TdmPrincipal.ServiceExecute(Sender: TService);
begin
  Log('Servi�o em Execu��o');

   while not Self.Terminated do
   begin
     Sleep(200);
     ServiceThread.ProcessRequests(True);
   end;
end;

procedure TdmPrincipal.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Log('Servi�o Pausado');
  Paused:= True;
end;

procedure TdmPrincipal.ServiceShutdown(Sender: TService);
begin
  Log('Servi�o Desligado');
end;

procedure TdmPrincipal.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Log('Servi�o Iniciado');
  Started:= True;
end;

procedure TdmPrincipal.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Log('Servi�o Parado');
  Stopped:= True;
end;


end.
