object dmPrincipal: TdmPrincipal
  OldCreateOrder = False
  DisplayName = 'Teste'
  BeforeInstall = ServiceBeforeInstall
  AfterInstall = ServiceAfterInstall
  BeforeUninstall = ServiceBeforeUninstall
  AfterUninstall = ServiceAfterUninstall
  OnContinue = ServiceContinue
  OnExecute = ServiceExecute
  OnPause = ServicePause
  OnShutdown = ServiceShutdown
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 198
  Width = 357
  object Conn: TFDConnection
    Params.Strings = (
      'DriverID=MSSQL'
      'Database=Caixa Eletronico'
      'Server=NJDEV013'
      'User_Name=sa')
    Connected = True
    LoginPrompt = False
    Left = 56
    Top = 16
  end
  object Query: TFDQuery
    Connection = Conn
    SQL.Strings = (
      'Select * from Usuarios'
      'Where COD_User = :cod')
    Left = 104
    Top = 24
    ParamData = <
      item
        Name = 'COD'
        DataType = ftInteger
        ParamType = ptInput
        Value = 2
      end>
  end
end
