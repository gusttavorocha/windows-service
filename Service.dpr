program Service;

uses
  Vcl.SvcMgr,
  Principal in 'Principal.pas' {Service12: TService},
  LogUtils in 'LogUtils.pas';

{$R *.RES}

begin
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;

  Application.CreateForm(TdmPrincipal, dmPrincipal);
  Application.Run;
end.
